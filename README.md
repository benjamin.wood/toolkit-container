# Toolkit Container

This Docker Compose configuration can be used to run the Developer Toolkit inside a Docker container. The toolkit can then control Docker running on the host via the mounted socket (/var/run/docker.sock).

This should be considered a proof-of-concept, and could certainly be developed further and/or directly integrated with the Developer Toolkit.

Setup:

1. Clone this repository & `cd` to it.
2. `docker-compose run --rm handydev bash`
3. `./init`
4. Follow the Developer Toolkit setup prompts [as usual](https://gitlab.shared.handy-internal.com/engineering/developer-toolkit/blob/master/README.md#run-install-script)
5. Run `hdy` commands inside the container as usual
6. Consider attaching VS Code to the handydev service (the service this repo's docker-compose configuration), with VS Code Remote containers & opening a repo (at `/home/handy/handydev/repos`).

The more advanced (but useful) way to use VS Code inside containers is to create a dedicated `code` docker-compose service for the project(s) you work on.

Reach out to me on slack (@benjaminwood) if there is any way I can help improve this or assist with it's implementation directly in the Developer Toolkit.

Caveats
* Docker compose configuration assumes users private key is stored at ~/.ssh/id-rsa. If this is not the case for you, you'll need to modify the path.