#!/bin/sh
set -e

if ! [ -d "$HOME/.ssh" ]; then
  mkdir "$HOME/.ssh"
fi

sudo chmod 0700 "$HOME/.ssh"

if ! [ -f "$HOME/.ssh/config" ]; then
  echo "Host *.handy-internal.com\n\tStrictHostKeyChecking no\n" >> $HOME/.ssh/config
  echo "Host github.com\n\tStrictHostKeyChecking no\n" >> $HOME/.ssh/config
fi

if [ -f /run/secrets/user_ssh_key ]; then
  if ! [ -f "$HOME/.ssh/id_rsa" ]; then
    cp /run/secrets/user_ssh_key "$HOME/.ssh/id_rsa"
  fi
  chmod 0400 "$HOME/.ssh/id_rsa"
fi

if ! [ -w /var/run/docker.sock ]; then
  sudo chown handy:20 /var/run/docker.sock
fi

exec "$@"