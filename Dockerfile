FROM ubuntu:20.04

LABEL maintainer="Benjamin Wood <ben@hint.io>"

ARG DEBIAN_FRONTEND=noninteractive

# Install zip
RUN apt-get update && apt-get install -y \
  software-properties-common \
  curl \
  zip \
  xz-utils \
  gnupg2 \
  lsb-release \
  sudo \
  git \
  vim \
  ruby-full

RUN curl -sSL https://get.docker.com/ | sh

RUN curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
  chmod +x /usr/local/bin/docker-compose

###############################################################################
# Railsdock non-root user
###############################################################################

ARG UID=1000
ENV UID $UID
ARG GID=20
ENV GID $GID
ARG USER=handy
ENV USER $USER

RUN useradd -u $UID -g $GID -m $USER && \
    usermod -p "*" $USER && \
    usermod -aG sudo $USER && \
    echo "$USER ALL=NOPASSWD: ALL" >> /etc/sudoers.d/50-$USER

RUN mkdir /home/handy/handydev && chown $USER:$GID /home/handy/handydev
RUN mkdir /home/handy/.ssh && chown $USER:$GID /home/handy/.ssh

USER $USER

RUN sudo groupmod -g 998 docker && \
    sudo usermod -aG docker $USER

# Install Nix
RUN curl -L https://nixos.org/nix/install | sh && \
  echo "source /home/handy/.nix-profile/etc/profile.d/nix.sh" >> ~/.bashrc

COPY bin/init /home/handy/init
COPY entrypoint.sh /entrypoint.sh

WORKDIR /home/handy